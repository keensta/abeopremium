package com.lrns123.abeopremium.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.lrns123.abeopremium.AbeoPremium;

public class SpawnerMine implements Listener {

	@SuppressWarnings("unused")
	private final AbeoPremium plugin;

	public SpawnerMine(AbeoPremium plugin) {
		this.plugin = plugin;

	}

	@SuppressWarnings("deprecation")
    @EventHandler
	public void onBlockBreak(BlockBreakEvent ev) {
		if (ev.getBlock().getType() == Material.MOB_SPAWNER) {
			if (ev.getPlayer().hasPermission("abeopremium.spawner")) {
				final Block b = ev.getBlock();
				final Location bloc = ev.getBlock().getLocation();
				final int i = b.getTypeId();
				b.setType(Material.AIR);
				b.getWorld().dropItem(bloc, new ItemStack(i));
				ev.setCancelled(true);
			}
		}
	}

}
