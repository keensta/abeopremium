package com.lrns123.abeopremium.listeners;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;

import com.lrns123.abeopremium.AbeoPremium;
import com.lrns123.abeopremium.configuration.XPBoostConfig;

public class APEntityListener implements Listener {

	private final AbeoPremium plugin;
	private final Permission perms;
	private final XPBoostConfig cfg;

	public APEntityListener(AbeoPremium instance) {
		plugin = instance;
		perms = plugin.getPermissions();
		cfg = plugin.getXPBoostConfig();
	}

	@EventHandler
	public void onPlayerExpChange(PlayerExpChangeEvent ev) {
		final String[] groups = cfg.getGroups();
		double multiplier = 1;

		for (final String group : groups) {
			if (perms.playerInGroup(ev.getPlayer(), group))
			{
				multiplier = cfg.getExpMultiplier(group);
				break;
			}
		}

		int exp = ev.getAmount();
		exp = (int) Math.ceil(exp * multiplier);
		ev.setAmount(exp);
	}
}
