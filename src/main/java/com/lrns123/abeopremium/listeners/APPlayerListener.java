package com.lrns123.abeopremium.listeners;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import com.lrns123.abeopremium.AbeoPremium;
import com.lrns123.abeopremium.configuration.NetherCartConfig;

public class APPlayerListener implements Listener {

	private final AbeoPremium plugin;
	private final Chat chat;
	@SuppressWarnings("unused")
	private final NetherCartConfig cfg;

	public APPlayerListener(AbeoPremium instance) {
		plugin = instance;
		chat = plugin.getChat();
		cfg = plugin.getNetherCartConfig();
	}

	// Player list color
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {

		final Player ply = event.getPlayer();
		String tmp = chat.getPlayerPrefix(ply).replaceAll("(?i)&([0-9A-F])", "\u00A7$1") + ply.getDisplayName();

		if (chat.getPlayerPrefix(ply).contains("&")) {
			final int Pp = chat.getPlayerPrefix(ply).lastIndexOf("&");
			final String NPp = "&" + chat.getPlayerPrefix(ply).charAt(Pp + 1);
			tmp = NPp.replaceAll("(?i)&([0-9A-F])", "\u00A7$1") + ply.getDisplayName();
		}

		if (chat.getPlayerPrefix(ply).contains("M") || chat.getPlayerPrefix(ply).contains("A")) {
			if (chat.getPlayerPrefix(ply).contains("GM")) {
				tmp = "&3".replaceAll("(?i)&([0-9A-F])", "\u00A7$1") + ply.getDisplayName();
			} else {
				tmp = "&3".replaceAll("(?i)&([0-9A-F])", "\u00A7$1") + ply.getDisplayName();
			}

		}
		if (tmp.length() > 14) {
			tmp = tmp.substring(0, 12);
			tmp = tmp + "�\u00A7f";
		} else {
			tmp = tmp + "\u00A7f";
		}

		ply.setPlayerListName(tmp);
	}
}
