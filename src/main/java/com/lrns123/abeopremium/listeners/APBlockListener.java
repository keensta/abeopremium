package com.lrns123.abeopremium.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import com.lrns123.abeopremium.AbeoPremium;

public class APBlockListener implements Listener {

	@SuppressWarnings("unused")
	private final AbeoPremium plugin;

	public APBlockListener(AbeoPremium instance) {
		plugin = instance;
	}

	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		final Player ply = event.getPlayer();
		if (ply != null) {
			if (ply.hasPermission("abeopremium.sign.color")) {
				for (int i = 0; i < 4; i++) {
					event.setLine(i, event.getLine(i).replaceAll("(?i)&([0-9A-FKLMNOR])", "\u00A7$1"));
				}
			}
		}
	}
}
