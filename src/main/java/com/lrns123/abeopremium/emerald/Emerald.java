package com.lrns123.abeopremium.emerald;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import com.lrns123.abeopremium.AbeoPremium;
import com.lrns123.abeopremium.db.DBManager;
import com.lrns123.abeopremium.db.DBTransaction;

public class Emerald implements CommandExecutor, Listener {
	
	private HashMap<String, EmeraldPlayer> players = new HashMap<String, EmeraldPlayer>();
    private HashSet<String> pendingUnwinged = new HashSet<String>();
    private List<Material> banned = new LinkedList<Material>();

    private HashMap<String, Block> disguised = new HashMap<String, Block>();
    private HashMap<Block, String> disguiseBlocks = new HashMap<Block, String>();
    
    private AbeoPremium plugin;
    private DBManager dbManager;
    private Logger log = Logger.getLogger("Minecraft");
    
    public Emerald(AbeoPremium plugin) {
    	this.plugin = plugin;
    	this.dbManager = plugin.getDbManager();
    	
    	plugin.getCommand("reloademerald").setExecutor(this);
    	plugin.getCommand("bd").setExecutor(this);
    	plugin.getCommand("wings").setExecutor(this);
    	plugin.getCommand("hat").setExecutor(this);

    	plugin.getServer().getPluginManager().registerEvents(this, plugin);
    	
    	getEmeraldData();
    	setBanned();
    }
    
    private void setBanned() {
        banned.add(Material.TNT);
        banned.add(Material.LAVA);
        banned.add(Material.STATIONARY_LAVA);
        banned.add(Material.WATER);
        banned.add(Material.STATIONARY_WATER);
        banned.add(Material.DRAGON_EGG);
        banned.add(Material.FIRE);
        banned.add(Material.ENDER_PORTAL);
        banned.add(Material.PORTAL);
        banned.add(Material.PAINTING);
        banned.add(Material.AIR);
        banned.add(Material.CAKE_BLOCK);
        banned.add(Material.MOB_SPAWNER);
        banned.add(Material.SAPLING);
        banned.add(Material.BEACON);
        banned.add(Material.COMMAND);
        banned.add(Material.COMMAND_MINECART);
    }
    
    public void getEmeraldData() {
    	if (!dbManager.hasConnection()) {
            log.severe("[AbeoPremium] Could not load Emerald rank information. No database connection available");
            return;
    }

    Connection conn = null;

    try {
            conn = dbManager.getConnection();
            final DatabaseMetaData meta = conn.getMetaData();

            if (!meta.getTables(null, null, "abeo_emerald", null).next()) {
                    final Statement stat = conn.createStatement();
                    log.info("[AbeoPremium] Creating table 'abeo_emerald'");
                    stat.execute("CREATE TABLE `abeo_emerald` (name VARCHAR(32) NOT NULL UNIQUE, lastWing INT(11) NOT NULL)");
                    stat.close();
            }
            players.clear();

            final Statement stat = conn.createStatement();
            final ResultSet result = stat.executeQuery("SELECT `name`, `lastWing` FROM `abeo_emerald`");
            while (result.next()) {
                    final EmeraldPlayer record = new EmeraldPlayer(result.getString(1));
                    @SuppressWarnings("unused")
					final String name = result.getString(1);
                    record.setLastWing(result.getLong(2));
                    players.put(record.getPlayerName(), record);
            }
            result.close();
            stat.close();
    } catch (final Exception e) {
            log.severe("[AbeoPremium] Error occured while loading emerald rank information: " + e.getMessage());
            e.printStackTrace();
    } finally {
            if (conn != null) {
                    try {
                            conn.close();
                    } catch (final SQLException e) {
                    }
            }
    	}
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
            if (cmd.getName().equalsIgnoreCase("reloademerald")) {
                    if (sender.hasPermission("abeopremium.admin")) {
                            sender.sendMessage(ChatColor.BLUE + "Reloading emerald rank data...");
                            dbManager.forceQueue();
                            getEmeraldData();
                            sender.sendMessage(ChatColor.BLUE + "Reloading complete");
                            return true;
                    }
            }
            
            if (!(sender instanceof Player)) {
                return true;
            }

            if (!players.containsKey(sender.getName()))
            		return true;

            final EmeraldPlayer data = players.get(sender.getName());
            if (cmd.getName().equalsIgnoreCase("wings")) {
                if (args.length < 1) {
                        sender.sendMessage(ChatColor.RED + "Please specify a target");
                } else {
                        if ((System.currentTimeMillis() / 1000) - data.getLastWing() < 7200) {
                                final long timeRemaining = (7200 - ((System.currentTimeMillis() / 1000) - data.getLastWing())) / 60;
                                sender.sendMessage(ChatColor.RED + "You can only give wings once per 2 hours");
                                sender.sendMessage(ChatColor.RED + "" + (timeRemaining + 1) + " minute(s) remaining");
                        } else {

                                final Player target = Bukkit.getPlayerExact(args[0]);
                                if(target == null || !target.isOnline()) {
                                	sender.sendMessage(ChatColor.RED + "Target does not exist!");
                                	return true;
                                }
                                // NOTE: The lack of a space after concatArgs is intentional, to correct for the added space
                                Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " gives " + concatArgs(args) + "a pair of wings!");

                                final long lastWing = System.currentTimeMillis() / 1000;
                                final String playerName = sender.getName();
                                data.setLastWing(lastWing);

                                target.sendMessage(ChatColor.AQUA + "You can fly...  (30 Seconds)");
                                target.setAllowFlight(true);
                                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new WingExpireTask(this, target.getName()), 20 * 30);

                                dbManager.queueTransaction(new DBTransaction() {
                                    @Override
                                    public void executeQuery(Connection conn) {

                                            PreparedStatement stat;
                                            try {
                                                    stat = conn.prepareStatement("UPDATE `abeo_emerald` SET `lastWing`=? WHERE `name`=?");
                                                    stat.setLong(1, lastWing);
                                                    stat.setString(2, playerName);
                                                    stat.execute();
                                                    stat.close();
                                            } catch (final SQLException e) {
                                                    log.severe("[AbeoPremium] Failed to update Aether records");
                                                    e.printStackTrace();
                                            }

                                    }
                            });
                        }
                }
        } else if (cmd.getName().equalsIgnoreCase("bd")) {
                // Epic coding will ensue right here! :)

                if (!(sender instanceof Player)) {
                        return true;
                }

                if (disguised.containsKey(sender.getName())) {
                        final Player player = (Player) sender;
                        // Unhide player 'n stuff
                        final Block block = disguised.get(sender.getName());
                        disguiseBlocks.remove(block);
                        disguised.remove(sender.getName());
                        block.setType(Material.AIR);
                        sender.sendMessage(ChatColor.BLUE + "You have been undisguised");
                        for (final Player ply : Bukkit.getOnlinePlayers()) {
                                ply.showPlayer(player);
                        }

                } else {
                        if (args.length < 1) {
                                sender.sendMessage(ChatColor.RED + "Please specify the block to disguise as");
                        } else {
                                final Player player = (Player) sender;
                                final Block block = player.getWorld().getBlockAt(player.getLocation());
                                final Material bdisguise = Material.matchMaterial(args[0]);
                                if (block.getType() != Material.AIR) {
                                        sender.sendMessage(ChatColor.RED + "You cannot disguise yourself as a block here");
                                } else {
                                        if (bdisguise == null) {
                                                sender.sendMessage(ChatColor.RED + "Invalid Block");
                                        } else if (!(bdisguise.isBlock())) {
                                        	
                                                sender.sendMessage(ChatColor.RED + "This isn't a block. Try again.");
                                        } else if (banned.contains(bdisguise)) {
                                                sender.sendMessage(ChatColor.RED + "This block has been banned for use");
                                        } else {

                                                final Location ploc = player.getLocation();
                                                block.setType(bdisguise);
                                                disguised.put(player.getName(), block);
                                                disguiseBlocks.put(block, player.getName());
                                                player.teleport(ploc.add(1, 0, 0));

                                                for (final Player ply : Bukkit.getOnlinePlayers()) {
                                                        ply.hidePlayer(player);
                                                }

                                                sender.sendMessage(ChatColor.BLUE + "You have been disguised as a block.");

                                        }
                                }

                        }
                }
        }
            
        if(cmd.getLabel().equalsIgnoreCase("hat")) {
            if (!(sender instanceof Player)) {
                return true;
            }
            
            if(args.length == 0) {
            	Player ply = (Player) sender;
            	ItemStack hand = ply.getItemInHand();
            	ItemStack helmet = ply.getInventory().getHelmet();
            
            	if(!hand.getType().equals(Material.AIR)) {
            		ply.getInventory().setHelmet(hand);
            		ply.setItemInHand(helmet);
            		ply.sendMessage(ChatColor.BLUE + "You've placed the item upon your head.");
            	} else {
            		ply.sendMessage(ChatColor.RED + "You need to be holding an item to put it upon your head.");
            	}
            }
            
        }
            return false;
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent ev) {
            final Player ply = ev.getPlayer();

            if (pendingUnwinged.contains(ply.getName())) {
                    pendingUnwinged.remove(ply.getName());
                    wingExpired(ply.getName());
            }
            
            // Hide disguised players
            for (final String name : disguised.keySet()) {
                    final Player ply2 = Bukkit.getPlayerExact(name);
                    if (ply2 == null)
                            continue;

                    ply.hidePlayer(ply2);
            }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent ev) {
            final Player ply = ev.getPlayer();
            final Location ploc = ply.getLocation();
            final Block block = disguised.get(ply.getName());
            if (disguised.containsKey(ply.getName())) {
                    if (block.getLocation().distance(ploc) > 4) {
                            ply.teleport(block.getLocation().add(1, 0, 0));
                    } else {
                            return;
                    }

            }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent ev) {
            final Block block = ev.getBlock();
            if (disguiseBlocks.containsKey(block)) {
                    final String ply = disguiseBlocks.get(block);
                    final Player player = Bukkit.getPlayerExact(ply);
                    block.setType(Material.AIR);
                    disguised.remove(ply);
                    disguiseBlocks.remove(block);
                    player.sendMessage(ChatColor.BLUE + "A player broke your block. You have been undisguised!");
                    for (final Player ply2 : Bukkit.getOnlinePlayers()) {
                            ply2.showPlayer(player);
                    }
            }
    }
    
    @EventHandler
    public void onPistionExtend(BlockPistonExtendEvent ev) {
    	BlockFace bf = ev.getDirection();
    	Block oldB = null;
    	Location newB = null;
    	String playerName = "";
    	for (Block b : ev.getBlocks()) {
    		if(disguiseBlocks.containsKey(b)) {
    			newB = b.getRelative(bf).getLocation();
    			oldB = b;
    			playerName = disguiseBlocks.get(b);
    		}
    	}
    	
    	if(newB != null) {
    		disguised.remove(playerName);
    		disguiseBlocks.remove(oldB);
    		disguised.put(playerName, newB.getBlock());
    		disguiseBlocks.put(newB.getBlock(), playerName);
    	}
    }
    
    @EventHandler 
    public void onPistonRetract(BlockPistonRetractEvent ev) {
    	Block oldB = null;
    	Location newB = null;
    	String playerName = "";
    	Block b = ev.getBlock();
    		if(disguiseBlocks.containsKey(b)) {
    			newB = (Location) b.getLocation();
    			oldB = b;
    			playerName = disguiseBlocks.get(b);
    		}
    	
    	
    	if(newB != null) {
    		disguised.remove(playerName);
    		disguiseBlocks.remove(oldB);
    		disguised.put(playerName, newB.getBlock());
    		disguiseBlocks.put(newB.getBlock(), playerName);
    	}
    }
    
    @EventHandler
    public void onBlockPhysics(BlockPhysicsEvent ev) {
    	Block b = ev.getBlock();
    	if(disguiseBlocks.containsKey(b)) {
    		ev.setCancelled(true);
    		if(b.getType() == Material.SAND || b.getType() == Material.GRAVEL) {
            final String ply = disguiseBlocks.get(b);
            final Player player = Bukkit.getPlayerExact(ply);
            b.setType(Material.AIR);
            disguised.remove(ply);
            disguiseBlocks.remove(b);
            player.sendMessage(ChatColor.BLUE + "A player broke your block. You have been undisguised!");
            	for (final Player ply2 : Bukkit.getOnlinePlayers()) {
            		ply2.showPlayer(player);
            	}
    		}
    	}
    }
    
    @EventHandler
    public void onTargetEvent(EntityTargetEvent ev) {
    	if(ev.getTarget() instanceof Player) {
    		Player ply = (Player) ev.getTarget();
    		if(disguised.containsKey(ply.getName())) {
    			ev.setCancelled(true);
    		}
    	}
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent ev) {
            final Player ply = ev.getPlayer();
            final Block block = disguised.get(ply.getName());
            if (disguised.containsKey(ply.getName())) {
                    block.setType(Material.AIR);
                    disguised.remove(ply.getName());
                    for (final Player ply2 : Bukkit.getOnlinePlayers()) {
                            ply2.showPlayer(ply);
                    }
            }
            for (final String ply2 : disguised.keySet()) {
                    final Player disguised = Bukkit.getPlayerExact(ply2);
                    ply.showPlayer(disguised);
            }
    }
    
    
  //Removes the ability to fly
    public void wingExpired(final String player) {
            final Player ply = Bukkit.getPlayerExact(player);
            if (ply != null && ply.isOnline()) {
                    ply.sendMessage(ChatColor.AQUA + "Your wings fall apart!");
                    ply.setAllowFlight(false);
            } else {
                    pendingUnwinged.add(player);
            }
    }
    
    public void shutdownDisguise() {
    	for(Block block : disguiseBlocks.keySet()) {
    	final String ply = disguiseBlocks.get(block);
        final Player player = Bukkit.getPlayerExact(ply);
        block.setType(Material.AIR);
        disguised.remove(ply);
        disguiseBlocks.remove(block);
        if(player != null && player.isOnline()) {
        	for (final Player ply2 : Bukkit.getOnlinePlayers()) {
        			ply2.showPlayer(player);
        		}	
        	}
    	}
    }

    //Used for /wings command
    private String concatArgs(String[] args) {
            final StringBuilder sb = new StringBuilder();
            for (final String arg : args) {
                    sb.append(arg);
                    sb.append(" ");
            }

            return sb.toString();
    }
    
    //Used by buycraft
    public void addEmeraldPurchase(final String playerName) {
            players.put(playerName, new EmeraldPlayer(playerName));

            dbManager.queueTransaction(new DBTransaction() {
                    @Override
                    public void executeQuery(Connection conn) {

                            PreparedStatement stat;
                            try {
                                    stat = conn.prepareStatement("INSERT IGNORE INTO `abeo_emerald` (`name`, `lastWing`) VALUES (?,0)");
                                    stat.setString(1, playerName);
                                    stat.execute();
                                    stat.close();
                            } catch (final SQLException e) {
                                    log.severe("[AbeoPremium] Failed to update Emerald records");
                                    e.printStackTrace();
                            }

                    }
            });
    }

}
