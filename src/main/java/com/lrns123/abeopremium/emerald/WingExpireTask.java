package com.lrns123.abeopremium.emerald;

public class WingExpireTask implements Runnable {

	private final Emerald manager;
	private final String player;

	public WingExpireTask(Emerald manager, String player) {
		this.manager = manager;
		this.player = player;
	}

	@Override
	public void run() {
		manager.wingExpired(player);
	}

}
