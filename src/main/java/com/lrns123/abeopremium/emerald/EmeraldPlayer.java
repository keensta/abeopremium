package com.lrns123.abeopremium.emerald;

public class EmeraldPlayer {
	private final String playerName;
	private long lastWing = 0;

	public EmeraldPlayer(String name) {
		playerName = name;
	}

	//Getting Vars
	public long getLastWing() {
		return lastWing;
	}

	public String getPlayerName() {
		return playerName;
	}

	//Setting Vars
	public void setLastWing(long lastWing) {
		this.lastWing = lastWing;
	}
}
