package com.lrns123.abeopremium;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.lrns123.abeopremium.aether.Aether;
import com.lrns123.abeopremium.configuration.NetherCartConfig;
import com.lrns123.abeopremium.configuration.XPBoostConfig;
import com.lrns123.abeopremium.db.DBManager;
import com.lrns123.abeopremium.emerald.Emerald;
import com.lrns123.abeopremium.listeners.APBlockListener;
import com.lrns123.abeopremium.listeners.APEntityListener;
import com.lrns123.abeopremium.listeners.APPlayerListener;
import com.lrns123.abeopremium.listeners.SpawnerMine;
import com.lrns123.abeopremium.mage.MageManager;
import com.lrns123.abeopremium.permissions.PermissionsAbstraction;
import com.lrns123.abeopremium.premium.Premium;
import com.lrns123.abeopremium.transactions.BuyCraftHandler;
import com.lrns123.abeopremium.util.Config;

public class AbeoPremium extends JavaPlugin {

	Logger log = Logger.getLogger("Minecraft");

	// Permissions system abstraction layer
	PermissionsAbstraction perms = new PermissionsAbstraction(this);

	// Listeners
	private APPlayerListener playerListener = null;
	private APEntityListener entityListener = null;
	private APBlockListener blockListener = null;
	private SpawnerMine spawnermine = null;

	// Command handlers
	private BuyCraftHandler bcHandler = null;

	// Config managers
	private NetherCartConfig netherCfg = null;
	private XPBoostConfig xpboostCfg = null;

	// Special ranks
	private MageManager mageManager = null;
	private Aether aether = null;
	private Emerald emerald = null;
	private Premium premium = null;
	// Vault
	private Chat chat = null;
	private Permission permission = null;
	private Economy economy = null;

	// Database connection && Config
	private DBManager dbManager = null;
	private Config config = null;

	@Override
	public void onDisable() {
		if (dbManager != null)
			dbManager.shutdown();

		if (mageManager != null)
			mageManager.saveConfig();
		
		getEmerald().shutdownDisguise();
		
		log.info("[AbeoPremium] Disabled");
	}

	@Override
	public void onEnable() {
		final PluginManager pm = getServer().getPluginManager();

		// Export default config.yml
		initializeConfig();

		// Initialize economy and permissions interface
		if (!setupEconomy()) {
			log.severe("[AbeoPremium] Unable to find economy plugin!");
		}

		if (!setupPermissions()) {
			log.severe("[AbeoPremium] Unable to find permissions plugin!");
		}

		if (!setupChat()) {
			log.severe("[AbeoPremium] Unable to find permissions plugin!");
		}

		// Initialize configuration
		netherCfg = new NetherCartConfig(this);
		xpboostCfg = new XPBoostConfig(this);

		netherCfg.load();
		xpboostCfg.load();

		// Database manager && Config
		dbManager = new DBManager(this);
		config = new Config(this);

		// Initialize listeners
		playerListener = new APPlayerListener(this);
		entityListener = new APEntityListener(this);
		blockListener = new APBlockListener(this);
		spawnermine = new SpawnerMine(this);

		// Special ranks
		emerald = new Emerald(this);
		premium = new Premium(this);

		// Initialize command handlers
		bcHandler = new BuyCraftHandler(this);

		getCommand("apbuypackage").setExecutor(bcHandler);
		getCommand("apremovepackage").setExecutor(bcHandler);

		pm.registerEvents(blockListener, this);
		pm.registerEvents(entityListener, this);
		pm.registerEvents(playerListener, this);
		pm.registerEvents(spawnermine, this);
	}

	private boolean setupEconomy() {
		final RegisteredServiceProvider<Economy> economyProvider = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}

	private boolean setupPermissions() {
		final RegisteredServiceProvider<Permission> permissionProvider = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		if (permissionProvider != null) {
			permission = permissionProvider.getProvider();
		}
		return (permission != null);
	}

	private boolean setupChat() {

		final RegisteredServiceProvider<Chat> chatProvider = this.getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
		if (chatProvider != null) {
			chat = chatProvider.getProvider();
		}

		return (chat != null);
	}

	private void initializeConfig() {
		final File dataDirectory = this.getDataFolder();
		if (dataDirectory.exists() == false)
			dataDirectory.mkdirs();

		final File f = new File(getDataFolder(), "config.yml");
		if (f.canRead())
			return;

		log.info("[AbeoPremium] Writing default config.yml");
		final InputStream in = getClass().getResourceAsStream("/config.yml");

		if (in == null) {
			log.severe("[AbeoPremium] Could not find config.yml resource");
			return;
		} else {
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(f);
				final byte[] buf = new byte[512];
				int len;
				while ((len = in.read(buf)) > 0) {
					fos.write(buf, 0, len);
				}
			} catch (final IOException iox) {
				log.severe("[AbeoPremium] Could not export config.yml");
				return;
			} finally {
				if (fos != null)
					try {
						fos.close();
					} catch (final IOException iox) {
					}
				if (in != null)
					try {
						in.close();
					} catch (final IOException iox) {
					}
			}
			return;
		}

	}

	public Permission getPermissions() {
		return permission;
	}

	public Economy getEconomy() {
		return economy;
	}

	public Chat getChat() {
		return chat;
	}

	public NetherCartConfig getNetherCartConfig() {
		return netherCfg;
	}

	public XPBoostConfig getXPBoostConfig() {
		return xpboostCfg;
	}

	// TODO: Move this to a separate class
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("updateplayerlist")) {
			if (sender.hasPermission("abeopremium.playerlist.update")) {
				final Player[] players = getServer().getOnlinePlayers();
				for (final Player ply : players) {

					String tmp = chat.getPlayerPrefix(ply).replaceAll("(?i)&([0-9A-F])", "\u00A7$1") + ply.getDisplayName();

					if (chat.getPlayerPrefix(ply).contains("&")) {
						final int Pp = chat.getPlayerPrefix(ply).lastIndexOf("&");
						final String NPp = "&" + chat.getPlayerPrefix(ply).charAt(Pp + 1);
						tmp = NPp.replaceAll("(?i)&([0-9A-F])", "\u00A7$1") + ply.getDisplayName();
					}
					
					if (tmp.length() > 14) {
						tmp = tmp.substring(0, 13);
						tmp = tmp + "�\u00A7f";
					} else {
						tmp = tmp + "\u00A7f";
					}

					ply.setPlayerListName(tmp);
				}
				sender.sendMessage(ChatColor.BLUE + "Player list updated.");
				return true;
			}
		}

		return false;
	}


	public MageManager getMageManager() {
		return mageManager;
	}

	/**
	 * @return the aether
	 */
	public Aether getAether() {
		return aether;
	}
	
	/**
	 * @return the Database manager
	 */
	public DBManager getDbManager() {
		return dbManager;
	}
	
	/**
	 * @return The config
	 */
	public Config getConfigClass() {
	    return config;
	}

	/**
	 * @return the emerald
	 */
	public Emerald getEmerald() {
		return emerald;
	}
	
	/**
	 * @return the Premium
	 */
	public Premium getPremium() {
	    return premium;
	}
}
