package com.lrns123.abeopremium.transactions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.lrns123.abeopremium.AbeoPremium;
import com.lrns123.abeopremium.permissions.PermissionsAbstraction;
import com.lrns123.abeopremium.permissions.PermissionsAbstraction.PremiumRank;

public class BuyCraftHandler implements CommandExecutor, Listener {

	private final Logger log = Logger.getLogger("Minecraft");
	private final PermissionsAbstraction permAbstraction;
	private final AbeoPremium plugin;
	@SuppressWarnings("unused")
    private final Permission perms;

	public BuyCraftHandler(AbeoPremium plugin) {
		this.plugin = plugin;
		perms = plugin.getPermissions();
		permAbstraction = new PermissionsAbstraction(plugin);
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		// Command: apbuypackage <packageID> <name>
		if (command.getName().equalsIgnoreCase("apbuypackage")) {
			if (sender instanceof Player)
				return true;

			if (args.length != 2) {
				log.info("[AbeoPremium] Invalid apbuypackage invocation: " + args);
				return true;
			}

			final String pkg = args[0];
			final String user = args[1];

			log.info("[AbeoPremium] Handling package " + pkg + " for user " + user);

			final Player ply = Bukkit.getPlayerExact(user);
			
			if (ply == null) {
				log.severe("Received " + pkg + " package from " + user + ", but user is not online!");
				
				FileConfiguration cfg = getConfig("playerranks");
				
				List<String> packages = new ArrayList<String>();
				
				if(cfg.contains(user))
					packages = cfg.getStringList(user + ".packages");
				
				if(!packages.contains(pkg))
					packages.add(pkg);
				
				cfg.set(user + ".packages", packages);
				saveConfig(cfg, "playerranks");
				
				return true;
			}

			// Premium
			if (pkg.equalsIgnoreCase("emerald")) {
			    
				if (permAbstraction.isPremium(ply, 1)) {
					ply.sendMessage(ChatColor.RED + "Warning: You've bought a full premium package while already");
					ply.sendMessage(ChatColor.RED + "being a premium member. To go to a higher rank, please use");
					ply.sendMessage(ChatColor.RED + "the corresponding upgrade package. Please contact an admin!");
				}
				
				permAbstraction.setPremiumRank(ply, PremiumRank.PREMIUM);
				dispatchForumNotification(ply, PremiumRank.PREMIUM.getGroupName(), "add");
				plugin.getEmerald().addEmeraldPurchase(ply.getName());
				ply.sendMessage(ChatColor.GREEN + "Thank you very much for your purchase!");
				ply.sendMessage(ChatColor.GREEN + "You are now emerald premium member. Enjoy!");
				
			} else if(pkg.equalsIgnoreCase("supporter")) {
			    
			    permAbstraction.setPremiumRank(ply, PremiumRank.SUPPORTER);
			    dispatchForumNotification(ply, PremiumRank.SUPPORTER.getGroupName(), "add");
                ply.sendMessage(ChatColor.GREEN + "Thank you very much for your purchase!");
                ply.sendMessage(ChatColor.GREEN + "You are now a supporter. Enjoy!");
			    
			} else if(pkg.equalsIgnoreCase("premium")) { 
			   
			    permAbstraction.setPremiumRank(ply, PremiumRank.PREMIUM);
			    dispatchForumNotification(ply, PremiumRank.PREMIUM.getGroupName(), "add");
			    ply.sendMessage(ChatColor.GREEN + "Thank you very much for your purchase!");
                ply.sendMessage(ChatColor.GREEN + "You are now a premium member. Enjoy!");
			    
			} else {
				log.severe("[AbeoPremium] Unknown package: " + pkg);
			}
		}
		
		if(command.getName().equalsIgnoreCase("apremovepackage")) {
			
			if (sender instanceof Player)
				return true;

			if (args.length != 2) {
				log.info("[AbeoPremium] Invalid apremovepackage invocation: " + args);
				return true;
			}

			final String pkg = args[0];
			final String user = args[1];

			log.info("[AbeoPremium] Handling package " + pkg + " for user " + user + " (Removel)");
			
			final Player ply = Bukkit.getPlayerExact(user);
			
			if(ply == null) {
			    log.severe("Received " + pkg + " package from " + user + ", but user is not online!");
                
                FileConfiguration cfg = getConfig("playerranks");
                
                List<String> packages = new ArrayList<String>();
                
                if(cfg.contains(user))
                    packages = cfg.getStringList(user + ".packages");
                
                if(!packages.contains(pkg))
                    packages.add(pkg);
                
                cfg.set(user + ".packages", packages);
                saveConfig(cfg, "playerranks");
                
                return true;
			}
			
			if(pkg.equalsIgnoreCase("supporter")) {
			    
			    permAbstraction.removePremiumRank(ply, PremiumRank.SUPPORTER);
			    dispatchForumNotification(ply, PremiumRank.SUPPORTER.getGroupName(), "remove");
			    ply.sendMessage(ChatColor.RED + "Unfortantly your supporter rank and ended, all abilites removed");
			    
			} else {
			    log.severe("[AbeoPremium] Unknown package: " + pkg);
			}
			
		}

		return false;
	}

	private void dispatchForumNotification(String name, String rank, String mode) {
		final ForumNotify notify = new ForumNotify(name, rank, mode);
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, notify);
	}

	private void dispatchForumNotification(Player ply, String rank, String mode) {
		dispatchForumNotification(ply.getName(), rank, mode);
	}
	
	@EventHandler
	public void playerJoinEvent(PlayerJoinEvent ev) {
		FileConfiguration cfg = getConfig("playerranks");
		String playerName = ev.getPlayer().getName();
		
		if(cfg.contains(playerName)) {
			
			for(String pkg : cfg.getStringList(playerName + ".packages")) {				
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "apbuypackage " + pkg + " " + playerName);
			}
			
			cfg.set(playerName, null);
			saveConfig(cfg, "playerranks");
			
		}
		
	}
	
	private FileConfiguration getConfig(String name) {
		return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), name+".yml"));
	}

	private void saveConfig(FileConfiguration cfg, String name) {
		try {
			cfg.save(new File(plugin.getDataFolder(), name+".yml"));
		} catch (final IOException e) {
			Logger.getLogger("Minecraft").severe("[AbeoPremium] Failed to save playerranks.yml!");
			e.printStackTrace();
		}
	}

}
