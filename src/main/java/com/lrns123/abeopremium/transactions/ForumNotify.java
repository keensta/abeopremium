package com.lrns123.abeopremium.transactions;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;

public class ForumNotify implements Runnable {

	private final String name;
	private final String rank;
	private final String mode;

	public ForumNotify(String name, String rank, String mode) {
		this.name = name;
		this.rank = rank;
		this.mode = mode;
	}

	Logger log = Logger.getLogger("Minecraft");

	@Override
	public void run() {
		try {
			final URL url = new URL("http://abeomc.net/premium/index.php?pass=fJnCCDJ8g74XYXh3GdbawzpmUyjXNK&u=" + name + "&m=" + mode + "&package=" + rank);
			final URLConnection con = url.openConnection();

			// Get the response
			final BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));

			String response = "";
			String line;
			while ((line = rd.readLine()) != null) {
				response = response + line;
			}

			rd.close();
			log.info("[AbeoPremium] Forum notified of new rank, response: " + response);
		} catch (final Exception e) {
			log.severe("[AbeoPremium] Error sending forum notification");
			e.printStackTrace();
		}
	}

}
