package com.lrns123.abeopremium.mage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.BlockIterator;

import com.lrns123.abeopremium.AbeoPremium;

@SuppressWarnings("deprecation")
public class MageManager implements CommandExecutor, Listener {

	private final AbeoPremium plugin;
	private final Set<String> mages = new HashSet<String>();
	private final Map<String, Double> mana = new HashMap<String, Double>();
	private final Map<String, String> target = new HashMap<String, String>();
	private final Map<String, Long> cooldown = new HashMap<String, Long>();

	private final Set<Material> armorBlacklist = new HashSet<Material>();

	public MageManager(AbeoPremium plugin) {
		this.plugin = plugin;

		plugin.getCommand("add-mage").setExecutor(this);
		plugin.getCommand("remove-mage").setExecutor(this);
		plugin.getCommand("list-mages").setExecutor(this);
		plugin.getCommand("mana").setExecutor(this);
		plugin.getCommand("mage").setExecutor(this);

		plugin.getServer().getPluginManager().registerEvents(this, plugin);

		loadConfig();

		armorBlacklist.add(Material.LEATHER_HELMET);
		armorBlacklist.add(Material.IRON_HELMET);
		armorBlacklist.add(Material.GOLD_HELMET);
		armorBlacklist.add(Material.DIAMOND_HELMET);
		armorBlacklist.add(Material.CHAINMAIL_HELMET);

		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {

			@Override
			public void run() {
				for (final String name : mages) {
					if (Bukkit.getOfflinePlayer(name).isOnline()) {
						double manaToAdd = 5.0 / 60.0;

						final Player ply = Bukkit.getPlayerExact(name);
						final ItemStack[] armor = ply.getInventory().getArmorContents();
						final ItemStack held = ply.getItemInHand();

						for (final ItemStack item : armor) {
							if (armorBlacklist.contains(item.getType())) {
								manaToAdd = 0;
							}
						}

						if (held.getType() == Material.BOOK)
							manaToAdd *= 2.0;

						double newMana = mana.get(name);
						newMana += manaToAdd;
						if (newMana > 100.0)
							newMana = 100.0;

						mana.put(name, newMana);
					}
				}

			}
		}, 0, 20);
	}

	private void loadConfig() {
		final FileConfiguration cfg = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "mage.yml"));

		mages.clear();
		mana.clear();
		target.clear();

		if (cfg == null)
			return;

		if (cfg.contains("mages")) {
			mages.addAll(cfg.getStringList("mages"));
		}

		if (cfg.contains("mana")) {
			for (final String name : cfg.getConfigurationSection("mana").getKeys(false)) {
				mana.put(name, cfg.getDouble("mana." + name));
			}
		}

	}

	public void saveConfig() {
		final FileConfiguration cfg = new YamlConfiguration();

		cfg.set("mages", mages.toArray(new String[0]));

		for (final String name : mana.keySet()) {
			cfg.set("mana." + name, mana.get(name));
		}

		try {
			cfg.save(new File(plugin.getDataFolder(), "mage.yml"));
		} catch (final IOException e) {
			Logger.getLogger("Minecraft").severe("[AbeoPremium] Failed to save mage.yml!");
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("add-mage")) {
			if (sender.hasPermission("abeopremium.admin")) {
				if (args.length < 1) {
					sender.sendMessage(ChatColor.RED + "Please specify the name of the player to add");
				} else {
					final String name = args[0];
					if (mages.contains(name)) {
						sender.sendMessage(ChatColor.RED + name + " is already a mage.");
					} else {
						mages.add(name);
						mana.put(name, 100.0);
						sender.sendMessage(ChatColor.BLUE + name + " is now a mage.");
						saveConfig();
					}
				}
			}
		} else if (cmd.getName().equalsIgnoreCase("remove-mage")) {
			if (sender.hasPermission("abeopremium.admin")) {
				if (args.length < 1) {
					sender.sendMessage(ChatColor.RED + "Please specify the name of the player to remove");
				} else {
					final String name = args[0];
					if (!mages.contains(name)) {
						sender.sendMessage(ChatColor.RED + name + " is not a mage.");
					} else {
						mages.remove(name);
						mana.remove(name);
						sender.sendMessage(ChatColor.BLUE + name + " is no longer a mage.");
						saveConfig();
					}
				}
			}
		} else if (cmd.getName().equalsIgnoreCase("list-mages")) {
			final StringBuilder listOnline = new StringBuilder();
			final StringBuilder listOffline = new StringBuilder();
			boolean firstOnline = true;
			boolean firstOffline = true;

			for (final String name : mages) {
				if (Bukkit.getOfflinePlayer(name).isOnline()) {
					if (!firstOnline) {
						listOnline.append(", ");
					} else {
						firstOnline = false;
					}

					listOnline.append(name);

				} else {
					if (!firstOffline) {
						listOffline.append(", ");
					} else {
						firstOffline = false;
					}

					listOffline.append(name);
				}
			}

			sender.sendMessage(ChatColor.BLUE + "Mages online:");
			sender.sendMessage(listOnline.toString());
			sender.sendMessage(ChatColor.BLUE + "Mages offline:");
			sender.sendMessage(listOffline.toString());
		} else if (cmd.getName().equalsIgnoreCase("mana")) {
			if (mages.contains(sender.getName())) {
				sender.sendMessage(ChatColor.BLUE + "You have " + String.format("%.1f", mana.get(sender.getName())) + " mana.");
			}
		} else if (cmd.getName().equalsIgnoreCase("mage")) {
			if (mages.contains(sender.getName())) {
				if (plugin.getChat() == null) {
					sender.sendMessage(ChatColor.RED + "Could not find chat plugin. Please contact an administrator");
				} else {
					String primaryGroup = plugin.getPermissions().getPrimaryGroup((Player) sender);

					if (primaryGroup.isEmpty()) { // Workaround
						final String groups[] = plugin.getPermissions().getPlayerGroups((Player) sender);
						if (groups.length > 0) {
							primaryGroup = groups[0];
						}
					}
					final String defPrefix = plugin.getChat().getGroupPrefix((String) null, primaryGroup);

					plugin.getChat().setPlayerPrefix((String) null, sender.getName(), "&9Mage " + defPrefix);

					sender.sendMessage(ChatColor.BLUE + "Your name now has the mage prefix");
				}
			}
		}
		return true;
	}

	private boolean takeMana(String player, double amount) {
		if (mana.containsKey(player)) {
			double dMana = mana.get(player);
			if (dMana < amount) {
				return false;
			} else {
				dMana -= amount;
				mana.put(player, dMana);
				return true;
			}
		}
		return false;
	}

	private Player getPlayerTarget(Player player, int range) {

		final List<Entity> nearby = player.getNearbyEntities(range, range, range);
		final ArrayList<Player> players = new ArrayList<Player>();

		for (final Entity e : nearby) {
			if (e instanceof Player) {
				players.add((Player) e);
			}
		}

		Player target = null;
		final BlockIterator bItr = new BlockIterator(player, range);

		Block block;
		Location loc;
		int bx, by, bz;
		double ex, ey, ez;
		// loop through player's line of sight
		while (bItr.hasNext()) {
			block = bItr.next();
			bx = block.getX();
			by = block.getY();
			bz = block.getZ();
			// check for entities near this block in the line of sight
			for (final Player e : players) {
				loc = e.getLocation();
				ex = loc.getX();
				ey = loc.getY();
				ez = loc.getZ();
				if ((bx - .75 <= ex && ex <= bx + 1.75) && (bz - .75 <= ez && ez <= bz + 1.75) && (by - 1 <= ey && ey <= by + 2.5)) {
					// entity is close enough, set target and stop
					target = e;
					break;
				}
			}
		}

		return target;
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent ev) {
		if (!mana.containsKey(ev.getPlayer().getName()))
			return;

		if (ev.getAction() == Action.RIGHT_CLICK_AIR || ev.getAction() == Action.RIGHT_CLICK_BLOCK) {
			boolean takeItem = false;
			if (ev.getItem().getType() == Material.FLINT) {
				final Block targetBlock = ev.getPlayer().getTargetBlock(null, 80);

				if (targetBlock == null) {
					ev.getPlayer().sendMessage(ChatColor.RED + "Target too far away.");
				} else if (!takeMana(ev.getPlayer().getName(), 2)) {
					ev.getPlayer().sendMessage(ChatColor.RED + "You have insufficient mana.");
				} else {

					final Location target = targetBlock.getLocation();
					final Random rnd = new Random();
					for (int i = 0; i < 10; i++) {
						final Location newTarget = target.clone();
						newTarget.add((rnd.nextDouble() * 2) - 1, 0, (rnd.nextDouble() * 2) - 1);
						ev.getPlayer().getWorld().strikeLightning(newTarget);
					}

					ev.getPlayer().sendMessage(ChatColor.DARK_BLUE + "You cast lightning!");

					takeItem = true;
				}
			} else if (ev.getItem().getType() == Material.GOLD_INGOT) {
				final Block targetBlock = ev.getPlayer().getTargetBlock(null, 50);

				if (targetBlock == null) {
					ev.getPlayer().sendMessage(ChatColor.RED + "Destination too far away.");
				} else if (!takeMana(ev.getPlayer().getName(), 10)) {
					ev.getPlayer().sendMessage(ChatColor.RED + "You have insufficient mana.");
				} else {
					final Location target = targetBlock.getLocation();
					target.add(0, 2, 0);

					// Retain orientation
					target.setYaw(ev.getPlayer().getLocation().getYaw());
					target.setPitch(ev.getPlayer().getLocation().getPitch());

					ev.getPlayer().teleport(target);
					ev.getPlayer().sendMessage(ChatColor.YELLOW + "You cast blink!");

					takeItem = true;
				}
			} else if (ev.getItem().getType() == Material.RED_ROSE) {
				final Player target = getPlayerTarget(ev.getPlayer(), 40);

				if (cooldown.containsKey(ev.getPlayer().getName()) && cooldown.get(ev.getPlayer().getName()) - System.currentTimeMillis() > 0) {
					ev.getPlayer().sendMessage(ChatColor.RED + "You cannot use this spell so soon after the last use.");
				} else if (target == null) {
					ev.getPlayer().sendMessage(ChatColor.RED + "Invalid target.");
				} else if (!takeMana(ev.getPlayer().getName(), 10)) {
					ev.getPlayer().sendMessage(ChatColor.RED + "You have insufficient mana.");
				} else {
					double newHealth = target.getHealth() + 8;
					if (newHealth > target.getMaxHealth()) {
						newHealth = target.getMaxHealth();
					}
					target.setHealth(newHealth);

					Bukkit.broadcastMessage(ChatColor.AQUA + ev.getPlayer().getName() + " heals " + target.getName() + " with their magical abilities!");
					cooldown.put(ev.getPlayer().getName(), System.currentTimeMillis() + 3000);

					takeItem = true;
				}
			} else if (ev.getItem().getType() == Material.DIAMOND) {
				final Player target = getPlayerTarget(ev.getPlayer(), 40);

				if (target == null) {
					ev.getPlayer().sendMessage(ChatColor.RED + "Invalid target.");
				} else if (!takeMana(ev.getPlayer().getName(), 50)) {
					ev.getPlayer().sendMessage(ChatColor.RED + "You have insufficient mana.");
				} else {

					final Player ply = ev.getPlayer();
					target.hidePlayer(ply);
					ply.sendMessage(ChatColor.BLUE + "You are now invisible to " + target.getName());

					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

						@Override
						public void run() {
							if (target.isOnline() && ply.isOnline()) {
								target.showPlayer(ply);
								ply.sendMessage(ChatColor.BLUE + "You are no longer invisible to " + target.getName());
							}

						}
					}, 20 * 10);

					takeItem = true;
				}
			}

			if (takeItem) {
				final ItemStack item = ev.getPlayer().getItemInHand();
				if (item.getAmount() > 1) {
					item.setAmount(item.getAmount() - 1);
					ev.getPlayer().setItemInHand(item);
				} else {
					ev.getPlayer().setItemInHand(new ItemStack(0));
				}
			}
		}
	}
}
