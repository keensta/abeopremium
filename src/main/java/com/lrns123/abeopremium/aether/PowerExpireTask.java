package com.lrns123.abeopremium.aether;

public class PowerExpireTask implements Runnable {

	private final Aether manager;
	private final String player;

	public PowerExpireTask(Aether manager, String player) {
		this.manager = manager;
		this.player = player;
	}

	@Override
	public void run() {
		manager.powerExpired(player);
	}

}
