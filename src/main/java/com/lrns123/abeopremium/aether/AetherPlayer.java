package com.lrns123.abeopremium.aether;

public class AetherPlayer {
	private final String playerName;
	private long lastPower = 0;
	private long lastNameC = 0;
	private long lastWing = 0;

	public AetherPlayer(String name) {
		playerName = name;
	}

	//Getting Vars
	public long getLastPower() {
		return lastPower;
	}

	public long getLastNameC() {
		return lastNameC;
	}

	public long getLastWing() {
		return lastWing;
	}

	public String getPlayerName() {
		return playerName;
	}

	//Setting Vars
	public void setLastPower(long lastPower) {
		this.lastPower = lastPower;
	}

	public void setLastNameC(long lastNameC) {
		this.lastNameC = lastNameC;
	}

	public void setLastWing(long lastWing) {
		this.lastWing = lastWing;
	}
}
