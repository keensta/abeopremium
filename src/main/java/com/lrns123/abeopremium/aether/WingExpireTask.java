package com.lrns123.abeopremium.aether;

public class WingExpireTask implements Runnable {

	private final Aether manager;
	private final String player;

	public WingExpireTask(Aether manager, String player) {
		this.manager = manager;
		this.player = player;
	}

	@Override
	public void run() {
		manager.wingExpired(player);
	}

}
