package com.lrns123.abeopremium.aether;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.lrns123.abeopremium.AbeoPremium;
import com.lrns123.abeopremium.db.DBManager;
import com.lrns123.abeopremium.db.DBTransaction;
import com.lrns123.abeopremium.permissions.PermissionsAbstraction;

public class Aether implements CommandExecutor, Listener {

        private final HashMap<String, AetherPlayer> players = new HashMap<String, AetherPlayer>();
        private final HashMap<String, String> FnamesRF = new HashMap<String, String>();
        private final HashMap<String, String> FnamesFR = new HashMap<String, String>();
        private final HashMap<String, Integer> playerPowers = new HashMap<String, Integer>();
        private final HashSet<String> pendingUnpowered = new HashSet<String>();
        private final HashSet<String> pendingUnwinged = new HashSet<String>();
        private final List<Material> banned = new LinkedList<Material>();

        private final HashMap<String, Block> disguised = new HashMap<String, Block>();
        private final HashMap<Block, String> disguiseBlocks = new HashMap<Block, String>();

        private final AbeoPremium plugin;
        private final DBManager dbManager;
        private final Logger log = Logger.getLogger("Minecraft");

        public Aether(AbeoPremium plugin) {
                this.plugin = plugin;
                this.dbManager = plugin.getDbManager();
                new PermissionsAbstraction(plugin);

                plugin.getCommand("reloadaether").setExecutor(this);
                plugin.getCommand("who").setExecutor(this);
                plugin.getCommand("power").setExecutor(this);
                plugin.getCommand("newname").setExecutor(this);
              //plugin.getCommand("wings").setExecutor(this);
              //plugin.getCommand("bd").setExecutor(this);
                plugin.getCommand("claim").setExecutor(this);
                plugin.getCommand("aether").setExecutor(this);

                plugin.getServer().getPluginManager().registerEvents(this, plugin);

                getAetherData();
                setBanned();
        }

        private void setBanned() {
                banned.add(Material.TNT);
                banned.add(Material.LAVA);
                banned.add(Material.STATIONARY_LAVA);
                banned.add(Material.WATER);
                banned.add(Material.STATIONARY_WATER);
                banned.add(Material.DRAGON_EGG);
                banned.add(Material.FIRE);
                banned.add(Material.ENDER_PORTAL);
                banned.add(Material.PORTAL);
                banned.add(Material.PAINTING);
                banned.add(Material.AIR);
        }

        private void getAetherData() {
                if (!dbManager.hasConnection()) {
                        log.severe("[AbeoPremium] Could not load Aether rank information. No database connection available");
                        return;
                }

                Connection conn = null;

                try {
                        conn = dbManager.getConnection();
                        final DatabaseMetaData meta = conn.getMetaData();

                        if (!meta.getTables(null, null, "abeo_aether", null).next()) {
                                final Statement stat = conn.createStatement();
                                log.info("[AbeoPremium] Creating table 'abeo_aether'");
                                stat.execute("CREATE TABLE `abeo_aether` (name VARCHAR(32) NOT NULL UNIQUE, Fname VARCHAR(32) NOT NULL, lastPower INT(11) NOT NULL, lastNameC INT(11) NOT NULL, lastWing INT(11) NOT NULL)");
                                stat.close();
                        }
                        players.clear();
                        for (final int handle : playerPowers.values()) {
                                Bukkit.getScheduler().cancelTask(handle);
                        }
                        playerPowers.clear();

                        final long minTime = (System.currentTimeMillis() / 1000) - (15 * 60);

                        final Statement stat = conn.createStatement();
                        final ResultSet result = stat.executeQuery("SELECT `name`, `Fname`, `lastPower`, `lastNameC`, `lastWing` FROM `abeo_aether`");
                        while (result.next()) {
                                final AetherPlayer record = new AetherPlayer(result.getString(1));
                                final String name = result.getString(1);
                                final String Fname = result.getString(2);
                                final long startTime = result.getLong(3);
                                record.setLastPower(result.getLong(3));
                                record.setLastNameC(result.getLong(4));
                                record.setLastWing(result.getLong(5));
                                players.put(record.getPlayerName(), record);
                                FnamesFR.put(Fname, name);
                                FnamesRF.put(name, Fname);
                                if (startTime > minTime) {
                                        final long remTime = (15 * 60) - ((System.currentTimeMillis() / 1000) - startTime);

                                        final int handle = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new PowerExpireTask(this, name), remTime * 20);
                                        playerPowers.put(name, handle);

                                        final Player ply = Bukkit.getPlayerExact(name);
                                        if (ply != null) {

                                        } else {
                                                powerExpired(name);
                                        }
                                }
                        }
                        result.close();
                        stat.close();
                } catch (final Exception e) {
                        log.severe("[AbeoPremium] Error occured while loading aether rank information: " + e.getMessage());
                        e.printStackTrace();
                } finally {
                        if (conn != null) {
                                try {
                                        conn.close();
                                } catch (final SQLException e) {
                                }
                        }
                }
        }

        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
                if (cmd.getName().equalsIgnoreCase("reloadaether")) {
                        if (sender.hasPermission("abeopremium.admin")) {
                                sender.sendMessage(ChatColor.BLUE + "Reloading aether rank data...");
                                dbManager.forceQueue();
                                getAetherData();
                                sender.sendMessage(ChatColor.BLUE + "Reloading complete");
                                return true;
                        }
                } else if (cmd.getName().equalsIgnoreCase("who")) {
                        if (sender.hasPermission("abeopremium.admin")) {
                                if (args.length == 0) {
                                        sender.sendMessage(ChatColor.RED + "/who [Name]");
                                        return false;
                                }
                                if (FnamesFR.containsKey(args[0])) {
                                        final String realname = FnamesFR.get(args[0]);
                                        sender.sendMessage(ChatColor.BLUE + args[0] + " is " + realname);
                                        return true;
                                } else {
                                        sender.sendMessage(ChatColor.RED + "Failed to obtain name. Double check the name.");

                                }
                        }
                }

                if (!(sender instanceof Player)) {
                        return true;
                }

                if (!players.containsKey(sender.getName()))
                        return true;

                final AetherPlayer data = players.get(sender.getName());

                if (cmd.getName().equalsIgnoreCase("aether")) {
        			Bukkit.broadcastMessage(ChatColor.GRAY + sender.getName() + " flies away");
        		}
                
                if (cmd.getName().equalsIgnoreCase("power")) {
                        if ((System.currentTimeMillis() / 1000) - data.getLastPower() < 7200) {
                                final long timeRemaining = (7200 - ((System.currentTimeMillis() / 1000) - data.getLastPower())) / 60;
                                sender.sendMessage(ChatColor.RED + "You can only use aether power once per 2 hours");
                                sender.sendMessage(ChatColor.RED + "" + (timeRemaining + 1) + " minute(s) remaining");
                        } else {
                                sender.sendMessage(ChatColor.RED + "Fusrodah!");

                                final String playerName = sender.getName();
                                final long lastPower = System.currentTimeMillis() / 1000;
                                data.setLastPower(lastPower);

                                final int handle = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new PowerExpireTask(this, sender.getName()), 900 * 20);
                                playerPowers.put(sender.getName(), handle);

                                dbManager.queueTransaction(new DBTransaction() {
                                        @Override
                                        public void executeQuery(Connection conn) {

                                                PreparedStatement stat;
                                                try {
                                                        stat = conn.prepareStatement("UPDATE `abeo_aether` SET `lastPower`=? WHERE `name`=?");
                                                        stat.setLong(1, lastPower);
                                                        stat.setString(2, playerName);
                                                        stat.execute();
                                                        stat.close();
                                                } catch (final SQLException e) {
                                                        log.severe("[AbeoPremium] Failed to update Aether records");
                                                        e.printStackTrace();
                                                }

                                        }
                                });

                        }
                } else if (cmd.getName().equalsIgnoreCase("newname")) {
                        if (args.length == 0) {
                                sender.sendMessage(ChatColor.RED + "Try /newname [Name]");
                        } else {
                                if (((System.currentTimeMillis() / 1000) - data.getLastNameC()) < 604800) { //Attemping to make this work of mins instead of seconds. :/
                                        final long timeRemaining = (604800 - ((System.currentTimeMillis() / 1000) - data.getLastNameC())) / 60;
                                        sender.sendMessage(ChatColor.RED + "You can only change your name once per 7 Days");
                                        sender.sendMessage(ChatColor.RED + "" + (timeRemaining + 1) + " minute(s) remaining");
                                } else {
                                        final String newName = args[0];
                                        final String playerName = sender.getName();
                                        final long lastNameC = System.currentTimeMillis() / 1000;
                                        data.setLastNameC(lastNameC);

                                        Bukkit.getServer().getPlayerExact(sender.getName()).setDisplayName(newName);
                                        FnamesFR.put(newName, playerName);
                                        FnamesRF.put(playerName, newName);
                                        sender.sendMessage(ChatColor.BLUE + "You have changed your name to " + newName);
                                        dbManager.queueTransaction(new DBTransaction() {
                                                @Override
                                                public void executeQuery(Connection conn) {

                                                        PreparedStatement stat;
                                                        try {
                                                                stat = conn.prepareStatement("UPDATE `abeo_aether` SET `lastNameC`=?, `Fname`=? WHERE `name`=?");
                                                                stat.setLong(1, lastNameC);
                                                                stat.setString(2, newName);
                                                                stat.setString(3, playerName);
                                                                stat.execute();
                                                                stat.close();
                                                        } catch (final SQLException e) {
                                                                log.severe("[AbeoPremium] Failed to update Aether records");
                                                                e.printStackTrace();
                                                        }

                                                }
                                        });

                                }
                        }
                }/* else if (cmd.getName().equalsIgnoreCase("wings")) {
                        if (args.length < 1) {
                                sender.sendMessage(ChatColor.RED + "Please specify a target");
                        } else {
                                if ((System.currentTimeMillis() / 1000) - data.getLastWing() < 7200) {
                                        final long timeRemaining = (7200 - ((System.currentTimeMillis() / 1000) - data.getLastPower())) / 60;
                                        sender.sendMessage(ChatColor.RED + "You can only give wings once per 2 hours");
                                        sender.sendMessage(ChatColor.RED + "" + (timeRemaining + 1) + " minute(s) remaining");
                                } else {

                                        final Player target = Bukkit.getPlayerExact(args[0]);
                                        
                                        // NOTE: The lack of a space after concatArgs is intentional, to correct for the added space
                                        Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " gives " + concatArgs(args) + "a pair of wings!");

                                        final long lastWing = System.currentTimeMillis() / 1000;
                                        final String playerName = sender.getName();
                                        data.setLastWing(lastWing);

                                        target.sendMessage(ChatColor.AQUA + "You can fly...  (30 Seconds)");
                                        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new WingExpireTask(this, target.getName()), 20 * 30);

                                        dbManager.queueTransaction(new DBTransaction() {
                                                @Override
                                                public void executeQuery(Connection conn) {

                                                        PreparedStatement stat;
                                                        try {
                                                                stat = conn.prepareStatement("UPDATE `abeo_aether` SET `lastWing`=? WHERE `name`=?");
                                                                stat.setLong(1, lastWing);
                                                                stat.setString(2, playerName);
                                                                stat.execute();
                                                                stat.close();
                                                        } catch (final SQLException e) {
                                                                log.severe("[AbeoPremium] Failed to update Aether records");
                                                                e.printStackTrace();
                                                        }

                                                }
                                        });
                                }
                        }
                } else if (cmd.getName().equalsIgnoreCase("bd")) {
                        // Epic coding will ensue right here! :)

                        if (!(sender instanceof Player)) {
                                return true;
                        }

                        if (disguised.containsKey(sender.getName())) {
                                final Player player = (Player) sender;
                                // Unhide player 'n stuff
                                final Block block = disguised.get(sender.getName());
                                disguiseBlocks.remove(block);
                                disguised.remove(sender.getName());
                                block.setType(Material.AIR);
                                sender.sendMessage(ChatColor.BLUE + "You have been undisguised");
                                for (final Player ply : Bukkit.getOnlinePlayers()) {
                                        ply.showPlayer(player);
                                }

                        } else {
                                if (args.length < 1) {
                                        sender.sendMessage(ChatColor.RED + "Please specify the block to disguise as");
                                } else {
                                        final Player player = (Player) sender;
                                        final Block block = player.getWorld().getBlockAt(player.getLocation());
                                        final Material bdisguise = Material.matchMaterial(args[0]);
                                        if (block.getType() != Material.AIR) {
                                                sender.sendMessage(ChatColor.RED + "You cannot disguise yourself as a block here");
                                        } else {
                                                if (bdisguise == null) {
                                                        sender.sendMessage(ChatColor.RED + "Invalid Block");
                                                } else if (!(bdisguise.isBlock())) {
                                                        sender.sendMessage(ChatColor.RED + "This isn't a block. Try again.");
                                                } else if (banned.contains(bdisguise)) {
                                                        sender.sendMessage(ChatColor.RED + "This block has been banned for use");
                                                } else {

                                                        final Location ploc = player.getLocation();
                                                        block.setType(bdisguise);
                                                        disguised.put(player.getName(), block);
                                                        disguiseBlocks.put(block, player.getName());
                                                        player.teleport(ploc.add(1, 0, 0));

                                                        for (final Player ply : Bukkit.getOnlinePlayers()) {
                                                                ply.hidePlayer(player);
                                                                //We storing  this in anytype of storage? Yea, disguised
                                                                // Undisguise upon logging
                                                        }

                                                        sender.sendMessage(ChatColor.BLUE + "You have been disguised as a block.");

                                                }
                                        }

                                }
                        }
                }*/

                return false;
        }

        @EventHandler
        public void onEntityDamage(EntityDamageEvent ev) {
                if (!(ev.getEntity() instanceof HumanEntity)) {

                        Player attacker = null;
                        if (ev instanceof EntityDamageByEntityEvent) {
                                final EntityDamageByEntityEvent ev2 = (EntityDamageByEntityEvent) ev;

                                if (ev2.getDamager() instanceof Player) {
                                        attacker = (Player) ev2.getDamager();
                                } else if (ev2.getDamager() instanceof Projectile) {
                                        final Projectile proj = (Projectile) ev2.getDamager();
                                        if (proj.getShooter() instanceof Player)
                                                attacker = (Player) proj.getShooter();
                                }
                        }

                        if (attacker != null) {
                                if (playerPowers.containsKey(attacker.getName())) {
                                        if ((System.currentTimeMillis() / 1000) - playerPowers.get(attacker.getName()) < 900) {
                                                ev.setDamage(ev.getDamage() * 2); // Damage buff
                                        } else {
                                                playerPowers.remove(attacker.getName());
                                        }
                                }
                        }
                }
        }

        @EventHandler
        public void onPlayerJoin(PlayerJoinEvent ev) {
                final Player ply = ev.getPlayer();

                if (pendingUnwinged.contains(ply.getName())) {
                        pendingUnwinged.remove(ply.getName());
                        wingExpired(ply.getName());
                }
                if (players.containsKey(ply.getName())) {
                        String Fakename = ply.getName();
                        if (FnamesRF.containsKey(ply.getName())) {
                                Fakename = FnamesRF.get(ply.getName());
                        }
                        //Location plyLoc = ply.getLocation();
                        Bukkit.getServer().getPlayerExact(ply.getName()).setDisplayName(Fakename);
                        ev.setJoinMessage(ChatColor.YELLOW + ply.getDisplayName() + " is hailed as they enter AbeoCraft!");
                        //plyLoc.getWorld().strikeLightningEffect(plyLoc);
                        if (pendingUnpowered.contains(ply.getName())) {
                                pendingUnpowered.remove(ply.getName());
                                powerExpired(ply.getName());
                        }
                }

                // Hide disguised players
                for (final String name : disguised.keySet()) {
                        final Player ply2 = Bukkit.getPlayerExact(name);
                        if (ply2 == null)
                                continue;

                        ply.hidePlayer(ply2);
                }
        }

        @EventHandler
        public void onPlayerMove(PlayerMoveEvent ev) {
                final Player ply = ev.getPlayer();
                final Location ploc = ply.getLocation();
                final Block block = disguised.get(ply.getName());
                if (disguised.containsKey(ply.getName())) {
                        if (block.getLocation().distance(ploc) > 3) {
                                ply.teleport(block.getLocation().add(1, 0, 0));
                        } else {
                                return;
                        }

                }
        }

        @EventHandler
        public void onBlockBreak(BlockBreakEvent ev) {
                final Block block = ev.getBlock();
                if (disguiseBlocks.containsKey(block)) {
                        final String ply = disguiseBlocks.get(block);
                        final Player player = Bukkit.getPlayerExact(ply);
                        block.setType(Material.AIR);
                        disguised.remove(ply);
                        disguiseBlocks.remove(block);
                        player.sendMessage(ChatColor.BLUE + "A player broke your block. You have been undisguised!");
                        for (final Player ply2 : Bukkit.getOnlinePlayers()) {
                                ply2.showPlayer(player);
                        }
                }
        }

        @EventHandler
        public void onPlayerQuit(PlayerQuitEvent ev) {
                final Player ply = ev.getPlayer();
                final Block block = disguised.get(ply.getName());
                if (disguised.containsKey(ply.getName())) {
                        block.setType(Material.AIR);
                        disguised.remove(ply.getName());
                        for (final Player ply2 : Bukkit.getOnlinePlayers()) {
                                ply2.showPlayer(ply);
                        }
                }
                for (final String ply2 : disguised.keySet()) {
                        final Player disguised = Bukkit.getPlayerExact(ply2);
                        ply.showPlayer(disguised);
                }
        }

        //Removes players powers
        public void powerExpired(final String player) {
                final Player ply = Bukkit.getPlayerExact(player);
                if (ply != null && ply.isOnline()) {
                        ply.sendMessage(ChatColor.DARK_AQUA + "The power fades away!");
                        playerPowers.remove(ply.getName());
                } else {
                        pendingUnpowered.add(player);
                }
        }

        //Removes the ability to fly
        public void wingExpired(final String player) {
                final Player ply = Bukkit.getPlayerExact(player);
                if (ply != null && ply.isOnline()) {
                        ply.sendMessage(ChatColor.AQUA + "Your wings fall apart!");
                        ply.setAllowFlight(false);
                } else {
                        pendingUnwinged.add(player);
                }
        }

        //Used for /wings command
        @SuppressWarnings("unused")
		private String concatArgs(String[] args) {
                final StringBuilder sb = new StringBuilder();
                for (final String arg : args) {
                        sb.append(arg);
                        sb.append(" ");
                }

                return sb.toString();
        }

        //Used by buycraft
        public void addAetherPurchase(final String playerName) {
                players.put(playerName, new AetherPlayer(playerName));

                dbManager.queueTransaction(new DBTransaction() {
                        @Override
                        public void executeQuery(Connection conn) {

                                PreparedStatement stat;
                                try {
                                        stat = conn.prepareStatement("INSERT IGNORE INTO `abeo_aether` (`name`, `Fname`, `lastPower`, `lastNameC`, `lastWing`) VALUES (?,?,0,0,0)");
                                        stat.setString(1, playerName);
                                        stat.setString(2, playerName);
                                        stat.execute();
                                        stat.close();
                                } catch (final SQLException e) {
                                        log.severe("[AbeoPremium] Failed to update Aether records");
                                        e.printStackTrace();
                                }

                        }
                });
        }

}