package com.lrns123.abeopremium.configuration;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.lrns123.abeopremium.AbeoPremium;

public class XPBoostConfig {

	private final AbeoPremium plugin;
	private HashMap<String, Double> groups;

	public XPBoostConfig(AbeoPremium instance) {
		plugin = instance;
	}

	public void load() {
		final FileConfiguration cfg = plugin.getConfig();
		final ConfigurationSection sec = cfg.getConfigurationSection("xpboost");

		groups = new HashMap<String, Double>();

		if (sec == null)
			return;

		final Map<String, Object> data = sec.getValues(false);

		if (data == null)
			return;

		for (final String key : data.keySet()) {
			try {
				final double multiplier = (Double) data.get(key);
				groups.put(key, multiplier);
			} catch (final Exception e) {

			}
		}
	}

	public String[] getGroups() {
		if (groups.size() == 0)
			return new String[0];
		else
			return groups.keySet().toArray(new String[0]);
	}

	public double getExpMultiplier(String group) {
		if (groups.containsKey(group))
			return groups.get(group);
		else
			return 1;

	}

}
