package com.lrns123.abeopremium.configuration;

import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import com.lrns123.abeopremium.AbeoPremium;

public class NetherCartConfig {

	private final AbeoPremium plugin;

	private String m_message;
	private List<String> m_worlds;

	public NetherCartConfig(AbeoPremium instance) {
		plugin = instance;
	}

	public void load() {
		final FileConfiguration cfg = plugin.getConfig();
		m_message = cfg.getString("nethercart.message", "").replaceAll("(?i)&([0-9A-F])", "\u00A7$1");
		
		if (cfg.isList("nethercart.worlds")) {
			m_worlds = cfg.getStringList("nethercart.worlds");
		}
	}

	public String getMessage() {
		return m_message;
	}

	public boolean affectWorld(String world) {
		if (m_worlds != null) {
			return m_worlds.contains(world);
		}
		return false;
	}

}
