package com.lrns123.abeopremium.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

public class DBTransactionHandler implements Runnable {

	private final Logger log = Logger.getLogger("Minecraft");
	private final Queue<DBTransaction> queue = new ConcurrentLinkedQueue<DBTransaction>();
	private final Lock lock = new ReentrantLock();
	private final DBManager manager;

	public DBTransactionHandler(DBManager manager) {
		this.manager = manager;
	}

	@Override
	public void run() {
		if (!lock.tryLock())
			return;

		Connection conn = null;

		try {
			conn = manager.getConnection();

			while (!queue.isEmpty()) {
				final DBTransaction trans = queue.poll();
				trans.executeQuery(conn);
			}

		} catch (final SQLException e) {
			log.severe("[AbeoPremium] Failed to execute query: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (final SQLException e) {
				}
			}
		}

		lock.unlock();
	}

	public void queueTransaction(DBTransaction transaction) {
		queue.add(transaction);
	}

}
