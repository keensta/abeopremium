package com.lrns123.abeopremium.db;

import java.sql.Connection;

public interface DBTransaction {
	void executeQuery(Connection conn);
}
