package com.lrns123.abeopremium.premium;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleMoveEvent;

import com.lrns123.abeopremium.AbeoPremium;

public class Premium implements CommandExecutor, Listener{
    
    AbeoPremium plugin;
    private List<String> fastPlayers = new ArrayList<String>();
    
    
    public Premium(AbeoPremium plugin) {
        this.plugin = plugin;
        
        plugin.getCommand("clouds").setExecutor(this);
        plugin.getCommand("fast").setExecutor(this);
        
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        
        if(!(sender instanceof Player))
            return true;
        
        if(!sender.hasPermission("abeopremium.premium"))
            return true;
        
        String cmd = command.getName();
        FileConfiguration cfg = plugin.getConfigClass().loadConfig("premium");
        Player p = (Player) sender;
        
        if(cmd.equalsIgnoreCase("clouds")) {
            
            long lastClouds = cfg.getLong(p.getName() + ".lastClouds");
            
            if((System.currentTimeMillis() / 1000) - lastClouds > 10800) {
                
                cfg.set(p.getName() + ".lastClouds", (System.currentTimeMillis() / 1000));
                plugin.getConfigClass().saveConfig(cfg, "premium");
                p.getWorld().setStorm(false);
                Bukkit.broadcastMessage(ChatColor.GREEN + p.getName() + " blows away all the clouds!");
                
            } else {
                
                long timeRemaining = (10800 - ((System.currentTimeMillis() / 1000) - lastClouds)) / 60;
                
                p.sendMessage(ChatColor.RED + "You can only use this once every 3 hours");
                p.sendMessage(ChatColor.RED + "" + (timeRemaining + 1) + " minute(s) remaining");
                
            }
            
        }
        
        if(cmd.equalsIgnoreCase("fast")) {
            
            if(fastPlayers.contains(p.getName())) {
                
                fastPlayers.remove(p.getName());
                p.sendMessage(ChatColor.GREEN + "You now have normal speed minecarts.");
                
            } else {
                
                fastPlayers.add(p.getName());
                p.sendMessage(ChatColor.GREEN + "You now have x2 speed minecarts.");
                
            }
            
        }
        
        
        return false;
    }
    
    @EventHandler
    public void onMinecartGo(VehicleMoveEvent ev) {
        
        if(ev.getVehicle().getType() == EntityType.MINECART) {
            
            if(!(ev.getVehicle().getPassenger() instanceof Player))
                return;
            
            Player p = (Player) ev.getVehicle().getPassenger();
            
            if(fastPlayers.contains(p.getName())) {
                Minecart mc = (Minecart) ev.getVehicle();
                
                mc.setMaxSpeed(0.5 * 2);
            }
            
        }
        
    }

}
