package com.lrns123.abeopremium.permissions;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.World;
import org.bukkit.entity.Player;

import com.lrns123.abeopremium.AbeoPremium;

public class PermissionsAbstraction {

	@SuppressWarnings("unused")
	private final AbeoPremium plugin;
	private final Permission perms;

	public enum PremiumRank {
		SUPPORTER("Supporter", 1), PREMIUM("Premium", 2), SEASONAL("seasonal", 3);

		private final String groupName;
		private final int tier;

		private PremiumRank(String groupName, int tier) {
			this.groupName = groupName;
			this.tier = tier;
		}

		public String getGroupName() {
			return groupName;
		}

		public int getTier() {
			return tier;
		}

	}

	public PermissionsAbstraction(AbeoPremium instance) {
		this.plugin = instance;
		this.perms = instance.getPermissions();
	}

	public void setPremiumRank(Player player, PremiumRank rank) {
		for (final PremiumRank i : PremiumRank.values()) {
			if (perms.playerInGroup(player, i.getGroupName())) {
				if (i.getTier() > rank.getTier()) {
					return;
				}

				perms.playerRemoveGroup(null, i.getGroupName());
			}
		}

		perms.playerAddGroup((World) null, player.getName(), rank.getGroupName());
	}
	
	public void removePremiumRank(Player player, PremiumRank rank) {
		if(perms.playerInGroup(player, rank.getGroupName())) {
			perms.playerRemoveGroup(player, rank.getGroupName());
		}
	}
	
	public void removeSeasonalRank(Player player, String rank) {
		if(perms.playerInGroup(player, rank)) {
			perms.playerRemoveGroup(player, rank);
		}
	}

	public boolean isPremium(Player player, int minTier) {
		for (final PremiumRank i : PremiumRank.values()) {
			if (perms.playerInGroup(player, i.getGroupName())) {
				if (i.getTier() > minTier) {
					return true;
				}
			}
		}
		return false;
	}
}